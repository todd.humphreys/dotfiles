#!/bin/bash

rm -rf $HOME/.vimrc
rm -rf $HOME/.vim
ln -s `pwd`/.vimrc $HOME/.vimrc
ln -s `pwd`/.vim $HOME/.vim
git config --global core.editor "vim"
