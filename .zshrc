# If you come from bash you might have to change your $PATH.
export PATH=.:/usr/local/lib:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(z git)

DISABLE_AUTO_UPDATE=true
source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"


# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
PATH=./:~/.local/bin:/usr/local/MATLAB/R2020b/bin:~/git/build/gss/shared/util:~/git/build/gss/shared/ppest:~/git/rscore/ppengine/src:~/git/gss/pprx/src:~/git/build/gss/pprx:~/git/build/rscore/ppengine:/usr/local/tmp:~/git/build/gss/shared/binflate:~/git/build/gss/shared/sp3cpolyfit:/usr/local/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/lib
alias ls="ls --color=auto --sort=extension --ignore-backups --group-directories-first"
alias gs="evince "
alias re="cd ~/research"
alias sr="cd ~/git/gss"
alias co="cd ~/git/gss/shared/core/src"
alias pp="cd ~/git/gss/pprx/src"
alias bu="cd ~/git/build"
alias fo="cd /vtrak2/group_c/data/NRT/2018"
alias db="cd ~/git/debug"
alias ppe="cd ~/git/rscore/ppengine/src"
alias ppet="cd ~/git/rscore/test"
alias pest="cd ~/git/gss/shared/ppest/src"
alias vnclt="sudo systemctl start vncserver@:3"
alias vnckill="sudo systemctl stop vncserver@:3"
alias vncgallium="vncserver :3 -localhost no -geometry 1920x1080"
alias vncgalliumkill="vncserver -kill :3"
alias pprxsb="~/git/buildsb/gss/pprx/pprx"
alias pprxdb="~/git/debug/gss/pprx/pprx"
alias ppenginedb="~/git/debug/rscore/ppengine/ppengine"
alias te="cd ~/teaching/gnssSignalProcessing"
alias fc="cd /fc"
alias c2="cd /fc/cabinet2"
alias dt="cd /dt"
alias sm="SumatraPDF > /dev0"
alias smd="SumatraPDF -invert-colors > /dev0"
alias ad="Acrobat"
alias ww="winword"
alias ppt="powerpnt"
alias xl="excel"
alias gpom="git push origin master"
alias gcom="git commit -a"
alias gcomm="git commit -a -m "wip""
alias xfig="export LC_ALL=C && xfig -startgridmode 2 -metric"
alias matlab="matlab -r clc -nosoftwareopengl"
DISABLE_AUTO_TITLE=true
export VISUAL=vim
export EDITOR="$VISUAL"

# ROS setup
if [[ $(hostname) == "iron" ]]; then
    source /opt/ros/noetic/setup.zsh
    source ~/Workspace/game-engine/build/devel/setup.zsh
fi

if [[ $(hostname) == "gallium" ]]; then
    source /opt/ros/noetic/setup.zsh
    source ~/project-primo/src/build/devel/setup.zsh
    ROS_MASTER_URI=http://10.42.0.131:11311
    ROS_IP=10.42.0.1
fi
