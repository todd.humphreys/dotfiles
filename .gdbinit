set auto-load safe-path /
# For some reason, the set commands below are met with a gdb error.  But you can
# enter them directly into a gdb session or in a gdb source file.  
# set history save on  # saves the command history between sessions
# set print pretty on  # displays values of classes, structs etc. nicely
# set confirm off      # you won't need to confirm commands
# python
# import sys
# sys.path.insert(0, '~/dotfiles')
# from printers import register_eigen_printers
# register_eigen_printers (None)
# end
