;; Todd's Emacs settings

;;----- Set load path
;; This is the path where emacs looks for custom lisp files
(add-to-list 'load-path "~/emacslisp")

;;----- AUC TeX
(load "auctex.el" nil t t)
;; Activate math mode in AUCTeX
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
;; Determines whether AUCTeX creates a PDF or DVI by default. 
(setq TeX-PDF-mode t)
(setq TeX-view-program-selection
      '((output-pdf "PDF Viewer")
        (output-dvi "DVI Viewer")))
(setq TeX-view-program-list
      '(("PDF Viewer" "SumatraPDF %o")
        ("DVI Viewer" "yap %o")))

;;----- Reftex activation (Reftex is included with Emacs 21.1 and later)
(autoload 'reftex-mode     "reftex" "RefTeX Minor Mode" t)
(autoload 'turn-on-reftex  "reftex" "RefTeX Minor Mode" nil)
(autoload 'reftex-citation "reftex-cite" "Make citation" nil)
(autoload 'reftex-index-phrase-mode "reftex-index" "Phrase mode" t)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)   ;; with AUCTeX LaTeX mode
(add-hook 'latex-mode-hook 'turn-on-reftex)   ;; with Emacs latex mode
(setq reftex-ref-macro-prompt nil)
;; Tell RefTeX where to find master bibliography file
(setq reftex-default-bibliography '("/cygdrive/c/Users/th9536/Documents/research/pangea/pangea.bib"))

;;----- Set up Matlab
(autoload 'matlab-mode "matlab" "Matlab Editing Mode" t)

;;----- Set up spell checking
(setq ispell-program-name "aspell")
(setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US"))

;;----- Global key bindings  
;; Define some functions to be bound to keys
(defun mle-list-buffers ()
  (interactive)
  (other-window 1)
  (list-buffers)
  (other-window 1))
(defun prev-window ()
  (interactive)
  (other-window -1))
;; Bind keys
(global-set-key [M-down] 'scroll-up)
(global-set-key [M-up] 'scroll-down)
(global-set-key (kbd "C-.") #'other-window)
(global-set-key (kbd "C-,") #'prev-window)
(global-set-key [C-tab] 'switch-to-buffer)
(global-set-key [f1] 'comment-region)
(global-set-key [f2] 'uncomment-region)
(global-set-key [f5] 'clang-format-buffer)
(global-set-key [?\C-x ?\C-b]  'mle-list-buffers)
(global-set-key [f9] 'goto-line)

;;----- Configure font lock syntax highlighting
(require 'modern-cpp-font-lock)
(modern-c++-font-lock-global-mode t)

;;----- Set up clang-format (doesn't seem to work  
;;(require 'clang-format)
;;(setq clang-format-style "file")

;;----- Other environment settings
(setq initial-frame-alist `((top + 0) (left + -7)))
(add-to-list 'default-frame-alist '(height . (text-pixels . 1051)))
(add-to-list 'default-frame-alist '(width . 88))
(setq-default fill-column 80)   
(set-background-color"black")
(set-foreground-color"blanched almond")
(set-cursor-color"yellow")
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(setq select-enable-primary t)
(setq-default truncate-lines t)
(setq-default truncate-partial-width-windows nil)
;; Force spaces instead of tabs
(setq-default indent-tabs-mode nil)
;; Auto-Fill-Mode on several Modes:
(add-hook 'text-mode-hook '(lambda () (auto-fill-mode 1)))
(add-hook 'TeX-mode-hook '(lambda () (auto-fill-mode 1)))
(add-hook 'LaTeX-mode-hook '(lambda () (auto-fill-mode 1)))
(add-hook 'HTML-mode-hook '(lambda () (auto-fill-mode 1)))
(add-hook 'Fundamental-mode-hook '(lambda () (auto-fill-mode 1)))
(add-hook 'c-mode-common-hook '(lambda () (auto-fill-mode 1)))

;;----- Set auto-mode-alist
;; Have emacs automatically select modes for files with these extensions
(setq my-auto-mode-alist
      (list
       '("_emacs" . emacs-lisp-mode)
       '("\\.make$" . makefile-mode)
       '("\\.common$" . makefile-mode)
       '("\\.opt$" . conf-unix-mode)
       '("\\.config$" . conf-unix-mode)
       '("\\.h$" . c++-mode)
       '("\\.H$" . c++-mode)
       '("\\.cpp$" . c++-mode)
       '("\\.cc$" . c++-mode)
       '("\\.CPP$" . c++-mode)
       '("\\.tcc$" . c++-mode)
       '("\\.cpf$" . c-mode)
       '("\\.CPF$" . c-mode)
       '("\\.m$" . matlab-mode)
       '("\\.txt$" . indented-text-mode)
       '("\\.TXT$" . indented-text-mode)
       '("\\.tex$" . LaTeX-mode)
       '("\\.TEX$" . LaTeX-mode)
       '("\\.mak$" . makefile-mode)
       '("\\.MAK$" . makefile-mode)
       '("\\.md$" . indented-text-mode)
       '("\\.nep" . emacs-lisp-mode)
       '("\\.made2" . emacs-lisp-mode)
       '("\\.dox" . doxymacs-mode))
      )
(setq auto-mode-alist
      (append my-auto-mode-alist auto-mode-alist))

;;----- CC-Mode settings
;; Default to c++ mode for .h files
(setq auto-mode-alist (cons '("\\.h\\'" . c++-mode ) auto-mode-alist))
;; Make a non-standard key binding.  We can put this in c-mode-base-map
;; because c-mode-map, c++-mode-map, and so on, inherit from it.
(defun my-c-initialization-hook ()
  (define-key c-mode-base-map [f6] 'compile)
  (define-key c-mode-base-map [f7] 'recompile)
)
(add-hook 'c-initialization-hook 'my-c-initialization-hook)
(require 'cc-mode)
(setq compilation-window-height 8)
(setq compilation-finish-function
      (lambda (buf str)
        (if (string-match "exited abnormally" str)
            ;;there were errors
            (message "compilation errors, press C-x ` to visit")
          ;;no errors, make the compilation window go away in 0.5 seconds
          (run-at-time 0.5 nil 'delete-windows-on buf)  ;; add if you like to delete the window
          (message "Compilation successful"))))
;; No indentation within namespaces
(defun my-c-setup ()
   (c-set-offset 'innamespace [0]))
(add-hook 'c++-mode-hook 'my-c-setup)

;;----- Automatically-generated custom settings
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t)
 '(matlab-indent-level 2)
 '(menu-bar-mode nil)
 '(mouse-drag-copy-region t)
 '(mouse-wheel-progressive-speed nil)
 '(safe-local-variable-values (quote ((TeX-master . "main.tex"))))
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Bitstream Vera Sans Mono" :foundry "outline" :slant normal :weight normal :height 98 :width normal)))))


(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
